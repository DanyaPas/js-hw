class TodosModel {
  constructor() {
    this.todos = [];
    this.apiUrl = "https://jsonplaceholder.typicode.com";
    this.filteredTodos = []
  }

  getTodos(cb) {
    axios.get(`${this.apiUrl}/todos`).then((res) => {
      this.todos = res.data;
      cb(this.todos);
    });
  }

  addTodo(title, cb) {
    const newTodo = {
      title,
      completed: false,
    };
    axios.post(`${this.apiUrl}/todos`, newTodo).then((res) => {
      console.log("RESPONSE", res);
      this.todos = [res.data, ...this.todos];
      cb(this.todos);
    });
  }

  removeTodo(id, cb) {
    axios.delete(`${this.apiUrl}/todos/${id}`).then((res) => {
      console.log("DELETED TODO", res);
      this.todos = this.todos.filter((todo) => todo.id !== id);
      cb(this.todos);
    });
  }

  switchFilter(type, cb) {
    if (type === "toDo") {
      this.filteredTodos = this.todos.filter((el) => {
        !el.completed
      })
    } else if (type === "compl") {
      this.filteredTodos = this.todos.filter((el) => {
        el.completed == true
      })
    } else {
      this.filteredTodos = this.todos
    }
    cb(this.filteredTodos)
  }

  switchCompleted(id, cb) {
    let todoToUpdate = this.todos.find((todo) => todo.id === id);
    todoToUpdate = {
      ...todoToUpdate,
      completed: !todoToUpdate.completed,
    };
    axios
      .put(`${this.apiUrl}/todos/${todoToUpdate.id}`, todoToUpdate)
      .then((res) => {
        this.todos = this.todos.map((todo) =>
          todo.id === todoToUpdate.id ? { ...res.data } : todo
        );
        console.log("res.data", res.data);
        cb(this.todos);
      });
  }
}

class TodosView {
  constructor(host) {
    this.host = host;
    this.formElement = this.host.querySelector(".todos-form");
    this.fieldElement = this.formElement.querySelector(".todos-form__field");
    this.listElement = this.host.querySelector(".todos-list");
    this.filter = this.host.querySelector(".todos-filter")
    this.filterBtn = this.filter.querySelector(".todos-filter__radio")
    this.allFilter = this.filter.querySelector(".all"),
    this.complFilter = this.filter.querySelector(".completed"),
    this.toDoFilter = this.filter.querySelector(".needToDo")
    this.removeBtnSelector = ".todo__remove";
    this.todoCheckboxSelector = ".todo__change-status";
  }

  clearInput() {
    this.fieldElement.value = "";
  }

  listenAdding(cb) {
    this.formElement.addEventListener("submit", (event) => {
      event.preventDefault();
      const title = this.fieldElement.value;
      if (title.length === 0) {
        alert("Введите title");
        return;
      }
      cb(title);
    });
  }

  listenFilterSwitching(cb) {
    this.allFilter.addEventListener("click", event => {
      cb("all")
    })
    
    this.complFilter.addEventListener("click", event => {
      cb("compl")
    })

    this.toDoFilter.addEventListener("click", event => {
      cb("toDo")
    })
  }

  filterRender(filter) {
    if (filter === "all") {
      allFilter.setAttribute("checked", "")
    } else if (filter === "compl") {
      complFilter.setAttribute("checked", "")
    } else {
      toDoFilter.setAttribute("checked", "")
    }
  }

  listenRemoving(cb) {
    this.listElement.addEventListener("click", (event) => {
      const target = event.target;
      if (target.matches(this.removeBtnSelector)) {
        if (target.dataset.todoId) {
          cb(+target.dataset.todoId);
        }
      }
    });
  }

  listenCompletedSwitching(cb) {
    this.listElement.addEventListener("change", (event) => {
      const target = event.target;
      if (target.matches(this.todoCheckboxSelector)) {
        if (target.dataset.todoId) {
          cb(+target.dataset.todoId);
        }
      }
    });
  }

  renderList(todos) {
    let template = "";
    todos.forEach((todo) => {
      template += `
        <li class="todos-list__item todo">
          <button class="todo__remove" data-todo-id="${todo.id}">X</button>
          <span>${todo.title}</span>
          <input data-todo-id="${todo.id
        }" class="todo__change-status" type="checkbox" ${todo.completed ? "checked" : ""
        }/>
        </li>
      `;
    });
    this.listElement.innerHTML = template;
  }
}
class TodosController {
  constructor(selector) {
    this.host = document.querySelector(selector);
    this.model = new TodosModel();
    this.view = new TodosView(this.host);
    this.init();
  }

  init() {
    this.model.getTodos(this.render.bind(this));

    this.listen();
  }

  listen() {
    this.view.listenAdding(this.addTodo.bind(this));
    this.view.listenRemoving(this.removeTodo.bind(this));
    this.view.listenCompletedSwitching(this.swicthCompleted.bind(this));
    this.view.listenFilterSwitching(this.switchFilter.bind(this));
  }

  render(todos) {
    this.view.renderList(todos);
  }

  addTodo(title) {
    this.model.addTodo(
      title,
      function (todos) {
        this.render(todos);
        this.view.clearInput();
      }.bind(this)
    );
  }

  switchFilter(id) {
    this.model.switchFilter(id, this.render.bind(this))
  }

  swicthCompleted(id) {
    this.model.switchCompleted(id, this.render.bind(this));
  }

  removeTodo(id) {
    this.model.removeTodo(id, this.render.bind(this));
  }
}

const todosController = new TodosController("#todosComponent");
