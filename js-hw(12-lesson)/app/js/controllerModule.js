import {TodosModel} from './modelModule.js';
import {TodosView} from './viewModule.js';

export class TodosController {
    constructor(selector) {
      this.host = document.querySelector(selector);
      this.model = new TodosModel();
      this.view = new TodosView(this.host);
      this.init();
    }
  
    init() {
      this.model.getTodos(this.render.bind(this));
  
      this.listen();
    }
  
    listen() {
      this.view.listenAdding(this.addTodo.bind(this));
      this.view.listenRemoving(this.removeTodo.bind(this));
      this.view.listenCompletedSwitching(this.swicthCompleted.bind(this));
      this.view.listenFilterSwitching(this.switchFilter.bind(this));
    }
  
    render(todos) {
      this.view.renderList(todos);
    }
  
    addTodo(title) {
      this.model.addTodo(
        title,
        function (todos) {
          this.render(todos);
          this.view.clearInput();
        }.bind(this)
      );
    }
  
    switchFilter(id) {
      this.model.switchFilter(id, this.render.bind(this))
    }
  
    swicthCompleted(id) {
      this.model.switchCompleted(id, this.render.bind(this));
    }
  
    removeTodo(id) {
      this.model.removeTodo(id, this.render.bind(this));
    }
  }