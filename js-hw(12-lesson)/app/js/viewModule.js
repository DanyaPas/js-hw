export class TodosView {
    constructor(host) {
      this.host = host;
      this.formElement = this.host.querySelector(".todos-form");
      this.fieldElement = this.formElement.querySelector(".todos-form__field");
      this.listElement = this.host.querySelector(".todos-list");
      this.filter = this.host.querySelector(".todos-filter")
      this.filterBtn = this.filter.querySelector(".todos-filter__radio")
      this.allFilter = this.filter.querySelector(".all"),
      this.complFilter = this.filter.querySelector(".completed"),
      this.toDoFilter = this.filter.querySelector(".needToDo")
      this.removeBtnSelector = ".todo__remove";
      this.todoCheckboxSelector = ".todo__change-status";
    }
  
    clearInput() {
      this.fieldElement.value = "";
    }
  
    listenAdding(cb) {
      this.formElement.addEventListener("submit", (event) => {
        event.preventDefault();
        const title = this.fieldElement.value;
        if (title.length === 0) {
          alert("Введите title");
          return;
        }
        cb(title);
      });
    }
  
    listenFilterSwitching(cb) {
      this.allFilter.addEventListener("click", event => {
        cb("all")
      })
      
      this.complFilter.addEventListener("click", event => {
        cb("compl")
      })
  
      this.toDoFilter.addEventListener("click", event => {
        cb("toDo")
      })
    }
  
    filterRender(filter) {
      if (filter === "all") {
        allFilter.setAttribute("checked", "")
      } else if (filter === "compl") {
        complFilter.setAttribute("checked", "")
      } else {
        toDoFilter.setAttribute("checked", "")
      }
    }
  
    listenRemoving(cb) {
      this.listElement.addEventListener("click", (event) => {
        const target = event.target;
        if (target.matches(this.removeBtnSelector)) {
          if (target.dataset.todoId) {
            cb(+target.dataset.todoId);
          }
        }
      });
    }
  
    listenCompletedSwitching(cb) {
      this.listElement.addEventListener("change", (event) => {
        const target = event.target;
        if (target.matches(this.todoCheckboxSelector)) {
          if (target.dataset.todoId) {
            cb(+target.dataset.todoId);
          }
        }
      });
    }
  
    renderList(todos) {
      let template = "";
      todos.forEach((todo) => {
        template += `
          <li class="todos-list__item todo">
            <button class="todo__remove" data-todo-id="${todo.id}">X</button>
            <span>${todo.title}</span>
            <input data-todo-id="${todo.id
          }" class="todo__change-status" type="checkbox" ${todo.completed ? "checked" : ""
          }/>
          </li>
        `;
      });
      this.listElement.innerHTML = template;
    }
  }
  