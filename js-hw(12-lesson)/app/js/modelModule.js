export class TodosModel {
    constructor() {
      this.todos = [];
      this.apiUrl = "https://jsonplaceholder.typicode.com";
      this.filteredTodos = []
    }
  
    getTodos(cb) {
      axios.get(`${this.apiUrl}/todos`).then((res) => {
        this.todos = res.data;
        cb(this.todos);
      });
    }
  
    addTodo(title, cb) {
      const newTodo = {
        title,
        completed: false,
      };
      axios.post(`${this.apiUrl}/todos`, newTodo).then((res) => {
        console.log("RESPONSE", res);
        this.todos = [res.data, ...this.todos];
        cb(this.todos);
      });
    }
  
    removeTodo(id, cb) {
      axios.delete(`${this.apiUrl}/todos/${id}`).then((res) => {
        console.log("DELETED TODO", res);
        this.todos = this.todos.filter((todo) => todo.id !== id);
        cb(this.todos);
      });
    }
  
    switchFilter(type, cb) {
      if (type === "toDo") {
        this.filteredTodos = this.todos.filter((el) => {
          !el.completed
        })
      } else if (type === "compl") {
        this.filteredTodos = this.todos.filter((el) => {
          el.completed == true
        })
      } else {
        this.filteredTodos = this.todos
      }
      cb(this.filteredTodos)
    }
  
    switchCompleted(id, cb) {
      let todoToUpdate = this.todos.find((todo) => todo.id === id);
      todoToUpdate = {
        ...todoToUpdate,
        completed: !todoToUpdate.completed,
      };
      axios
        .put(`${this.apiUrl}/todos/${todoToUpdate.id}`, todoToUpdate)
        .then((res) => {
          this.todos = this.todos.map((todo) =>
            todo.id === todoToUpdate.id ? { ...res.data } : todo
          );
          console.log("res.data", res.data);
          cb(this.todos);
        });
    }
  }
  