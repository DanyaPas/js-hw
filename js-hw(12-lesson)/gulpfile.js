const gulp = require("gulp");
const sass = require("gulp-sass");
const browserSync = require("browser-sync").create();
const webpack = require("webpack-stream");
const named = require("vinyl-named");

function style() {
  return gulp.src("app/scss/**/*.scss").pipe(sass()).pipe(gulp.dest("app/css"));
}

function js() {
  return gulp
    .src("./app/js/app.js")
    .pipe(named())
    .pipe(
      webpack({
        output: {
          filename: "[name].js",
        },
      })
    )
    .pipe(gulp.dest("./app"));
}

function server() {
  browserSync.init({
    server: {
      baseDir: "./app/html",
    },
  });
}

function watch() {
  gulp
    .watch("./app/scss/*.scss")
    .on("change", gulp.series(style, browserSync.reload));
  gulp.watch("./app/*.html").on("change", browserSync.reload);
  gulp
    .watch("./app/js/**/*.js")
    .on("change", gulp.series(js, browserSync.reload));
}

exports.default = gulp.series(
  js,
  gulp.parallel(server, watch)
);



// exports.default = gulp.parallel(js, server, watch)
