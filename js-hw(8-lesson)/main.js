class TodosComponent {
  constructor(selector) {
    this.host = document.querySelector(selector);
    this.formElement = this.host.querySelector(".todos-form");
    this.fieldElement = this.formElement.querySelector(".todos-form__field");
    this.listElement = this.host.querySelector(".todos-list");
    this.todosList = this.listElement.querySelectorAll(".todos-list__item")
    this.todos = [
      {
        title: "Some todo",
        done: false,
      },
      {
        title: "Some todo 2",
        done: true,
      },
    ];
    this.renderList();
    this.listen();
    this.completelisten();
  }

  addTodo(title) {
    const newTodo = {
      title,
      done: false,
    };
    this.todos = [...this.todos, newTodo];
    this.renderList()
  }

  listen() {
    this.formElement.addEventListener("submit", (event) => {
      event.preventDefault();
      const title = this.fieldElement.value;
      if (title.length === 0) {
        alert("Введите title");
        return;
      }
      this.addTodo(title)
    });
  }

  completelisten() {
    this.todosList.forEach((elem, i) => {
      this.todosList[i].addEventListener('select', function () {
        this.todos[i].done = !this.todos[i].done
      });
    });
  }

  renderList() {
    // создать HTML строку в которой будет находиться HTML со всеми элементами списка
    // с данными из this.todos внутри
    let template = "";
    this.todos.forEach((item) => {
      if (item.done === false) {
        template += `
        <li class="todos-list__item todo">
          <span>${item.title}</span>
          <input type="checkbox" />
        </li>`} else {
        template += `
          <li class="todos-list__item todo">
            <span>${item.title}</span>
            <input type="checkbox" checked/>
          </li>`
      }
    })
    this.listElement.innerHTML = template;
  }
}

const todosComponent = new TodosComponent("#todosComponent");
