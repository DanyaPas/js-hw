const products = [
    {
        id: 0,
        name: "iPhone 12",
        price: 800,
        amount: 0
    },
    {
        id: 1,
        name: "AirPods",
        price: 200,
        amount: 0
    },
    {
        id: 2,
        name: "Apple Watch",
        price: 600,
        amount: 0
    },
    {
        id: 3,
        name: "MacBook PRO",
        price: 3000,
        amount: 0
    },
];

class Cart {
    constructor() {
        this.products = []
    }
}

Cart.prototype.add = function(product) {
    this.products = [...this.products].filter((obj) => obj.id !== product.id)
    this.products = [...this.products, product];
    product.amount += 1
    let SumPrice = this.products.reduce((acc, curr) => (acc += curr.price * curr.amount), 0);
    console.log(this.products);
    console.log(`Полная цена равна: ${SumPrice}`);
}

Cart.prototype.remove = function(idToRemove) {

    if (products[idToRemove].amount > 1){
        products[idToRemove].amount -= 1
    } else {
        this.products = this.products.filter(
            (product) => product.id !== idToRemove
        );
    }
    let SumPrice = this.products.reduce((acc, curr) => (acc += curr.price * curr.amount), 0);
    console.log(this.products);
    console.log(`Полная цена равна: ${SumPrice}`);
}

const cart = new Cart()

cart.add(products[0])

cart.add(products[0])

cart.add(products[1])

cart.add(products[1])

cart.add(products[2])

cart.add(products[2])

cart.add(products[3])

cart.add(products[3])

cart.remove(3)

cart.remove(3)

cart.remove(2)

cart.remove(2)

cart.remove(1)

cart.remove(1)

cart.remove(0)

cart.remove(0)