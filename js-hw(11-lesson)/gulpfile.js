const { parallel } = require("gulp");
const gulp = require("gulp");
const sass = require("gulp-sass");
const browserSync = require("browser-sync").create();

function style() {
  return gulp.src("app/scss/**/*.scss").pipe(sass()).pipe(gulp.dest("app/css"));
}

function server() {
  browserSync.init({
    server: {
      baseDir: "./app",
    },
  });
}

function reload() {
    gulp.watch("app/*.html").on('change', browserSync.reload);
}

function watch() {
  gulp.watch("./app/scss/*.scss", style);
}

exports.style = style;
exports.server = server;
exports.reload = reload;
exports.watch = watch;
exports.start = parallel(watch, server, reload);
