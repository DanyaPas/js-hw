const products = [
    {
        id: 1,
        name: "iPhone 12",
        price: 800,
    },
    {
        id: 2,
        name: "AirPods",
        price: 200,
    },
    {
        id: 3,
        name: "Apple Watch",
        price: 600,
    },
    {
        id: 4,
        name: "MacBook PRO",
        price: 3000,
    },
];

function Cart() {
    return{
        products: [],
        add(product) {
            this.products = [...this.products, product];
            let SumPrice = this.products.reduce((acc, curr) => (acc += curr.price), 0);
            console.log(this.products);
            console.log(`Полная цена равна: ${SumPrice}`);
        },
        remove(idToRemove) {
            this.products = this.products.filter(
                (product) => product.id !== idToRemove
            );
            let SumPrice = this.products.reduce((acc, curr) => (acc += curr.price), 0);
            console.log(this.products);
            console.log(`Полная цена равна: ${SumPrice}`);
        },
    }
}

let cart = Cart()
cart.add(products[0])

cart.add(products[1])

cart.add(products[2])

cart.add(products[3])

cart.remove(1);

cart.remove(2);

cart.remove(3);

cart.remove(4);